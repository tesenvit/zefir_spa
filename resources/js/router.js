import Vue    from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        /**
        * --------------------------------------------------------------------------
        * Public
        * --------------------------------------------------------------------------
        * */
        {
            path: '/',
            name: 'home',
            meta: { layout: 'public' },
            component: () => import('./views/public/Home.vue')
        },
        {
            path: '/contacts',
            name: 'contacts',
            meta: { layout: 'public' },
            component: () => import('./views/public/Contacts.vue')
        },
        {
            path: '/blog',
            name: 'blog',
            meta: { layout: 'public' },
            component: () => import('./views/public/Blog.vue')
        },
        {
            path: '/gallery',
            name: 'gallery',
            meta: { layout: 'public' },
            component: () => import('./views/public/Gallery.vue')
        }

        /**
         * --------------------------------------------------------------------------
         * Auth
         * --------------------------------------------------------------------------
         * */


        /**
         * --------------------------------------------------------------------------
         * Admin
         * --------------------------------------------------------------------------
         * */
    ]
});
